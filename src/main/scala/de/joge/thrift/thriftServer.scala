package de.joge.thrift

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.hive.thriftserver.HiveThriftServer2

object thriftServer {
  def main(args: Array[String]): Unit = {
    //val conf = new SparkConf().setAppName("Thrift Server")
    //val sc = new SparkContext(conf)
    val spark = SparkSession.builder()
      .appName("Thrift Server")
      .enableHiveSupport()
      .config("hive.server2.thrift.port", "10001")
      .getOrCreate()
    //val sql = new SQLContext(sc)

    //sql.setConf("hive.server2.thrift.port", "10001")

    val df = spark.read.json("F:\\Workspace\\Spark-The-Definitive-Guide\\data\\flight-data\\json\\2015-summary.json")
    df.show(5)

    df.createOrReplaceGlobalTempView("flightData")
    HiveThriftServer2.startWithContext(spark.sqlContext)
    while (true) {
      Thread.`yield`()
    }
  }
}
